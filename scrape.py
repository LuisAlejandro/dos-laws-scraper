#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

import os
import re
import sys
import time
import json
import socket
import lxml.html
import lxml.etree
import unicodedata
from io import BytesIO
from datetime import datetime
from collections import OrderedDict
from argparse import ArgumentParser
from urllib.parse import urlparse, urlunparse, parse_qs, urlencode
from urllib.request import urlopen, HTTPError, URLError
from pymongo import MongoClient
from bson.objectid import ObjectId

if not sys.version_info < (3,):
    unicode = str
    basestring = str

# parse command line arguments
ap = ArgumentParser(description='DemocracyOS laws parser')
ap.add_argument('-l', action='store_true', help='load json into db')
ap.add_argument('-c', action='store_true', help='clean processed files')
ap.add_argument('-a', action='store_true', help='load all files')
ap.add_argument('-n', default=0, help='number of laws to index')
ap.add_argument('-m', default='support', choices=['vote', 'support', 'poll'],
                help='voting method')
args = ap.parse_args()

mongodb = 'DemocracyOS-dev'
mongohost = 'mongo'
mongoport = 27017

_months = {
    'Enero': 'January',
    'Febrero': 'February',
    'Marzo': 'March',
    'Abril': 'April',
    'Mayo': 'May',
    'Junio': 'June',
    'Julio': 'July',
    'Agosto': 'August',
    'Septiembre': 'September',
    'Octubre': 'October',
    'Noviembre': 'November',
    'Diciembre': 'December'
}

_slugify_strip_re = re.compile(r'[^\w\s-]')
_slugify_hyphenate_re = re.compile(r'[-\s]+')

_correct_text_re = [
    [r'º', r'°'],
    [r'\s+', r' '],
    [r'\s°', r'°'],
    [r'\s\.\s', r'.'],
    [r'\.\. ', r'.'],
    [r'Artículo', r'\nArtículo'],
    [r'Artículo\s(\d+)', r'Artículo \1'],
    [r'Artículo\s(\d+)[^°]', r'Artículo \1°'],
    [r'Artículo\s(\d+)°\.\.', r'Artículo \1°.'],
    [r'Artículo\s(\d+)°([^\.])', r'Artículo \1°.\2'],
    [r'Artículo\s(\d+)°\.\s*', r'Artículo \1°.'],
    [r'###\n', r'###'],
]

_correct_title_re = [
    [r'\s+', r' '],
]

_correct_summary_re = [
    [r'\s+', r' '],
]

_correct_art_re = [
    [r'\r\n', r' '],
    [r'\s+', r' '],
    [r'\¿', r'###'],
    [r'\?', r'###'],
]

_correct_final_re = [
    [r'###', r'"'],
]

mongoconn = MongoClient(mongohost, mongoport, connect=False)[mongodb]

dos_user = {
    'firstName': 'John',
    'lastName': 'Urrego',
    'username': 'ingenierofelipeurrego',
    'locale': 'es',
    'email': 'ingenierofelipeurrego@gmail.com',
    'emailValidated': True
}

dos_forum = {
    'name': 'camara',
    'title': 'Camara',
    'summary': '',
    'visibility': 'public'
}

dos_user_query = mongoconn['users'].find_one({'email': 'ingenierofelipeurrego@gmail.com'})

if dos_user_query:
    dos_user_id = str(dos_user_query.get('_id'))
else:
    dos_user_id = str(mongoconn['users'].insert_one(dos_user).inserted_id)

dos_forum_query = mongoconn['forums'].find_one({'name': 'camara'})

if dos_forum_query:
    dos_forum_id = str(dos_forum_query.get('_id'))
else:
    dos_forum_id = str(mongoconn['forums'].insert_one(dos_forum).inserted_id)


def u(u_string):
    if isinstance(u_string, unicode):
        return u_string
    return u_string.decode('utf-8')


def s(s_string):
    if isinstance(s_string, bytes):
        return s_string
    return s_string.encode('utf-8')


def _slugify(value):
    if not isinstance(value, unicode):
        value = unicode(value)
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = unicode(_slugify_strip_re.sub('', u(value)).strip().lower())
    return _slugify_hyphenate_re.sub('-', value)


def text_to_json(original, url, tag, lawID, publishedAt, closingAt, status,
                 state, pagename):
    if not os.path.exists(pagename):
        return

    print('Convirtiendo TXT a JSON ' + pagename)

    with open(pagename, 'rb') as f:
        fcontent = f.read()

    if len(fcontent) == 0:
        fcontent = '<body></body>'

    tree = lxml.etree.parse(BytesIO(fcontent), lxml.etree.HTMLParser())

    if tree.xpath('body/div[@class="Section1"]'):
        text = []
        clauses = []
        tag = u(tag)
        law_id = u(lawID)
        publishedAt = u(publishedAt)
        closingAt = u(closingAt)
        url = u(url)
        original = u(original)
        xpath_media_title = 'body/center/p/font/strong/text()'
        xpath_summary = 'body/center/text()'
        xpath_text = 'body/div[@class="Section1"]/p[@class="MsoNormal"]'
        media_title = u(tree.xpath(xpath_media_title)[0])
        summary = u(tree.xpath(xpath_summary)[1])

        for e in tree.xpath(xpath_text):
            _el = lxml.etree.tostring(e)
            _text_str = lxml.html.fragments_fromstring(_el)[0].text_content()
            for r, f in _correct_art_re:
                _text_str = re.sub(r, f, _text_str).strip()
            if _text_str:
                text.append(_text_str)

        text = ''.join(text)

        for r, f in _correct_text_re:
            text = re.sub(r, f, text).strip()

        for r, f in _correct_title_re:
            media_title = re.sub(r, f, media_title).strip(' .')

        for r, f in _correct_summary_re:
            summary = re.sub(r, f, summary).strip()

        if 'DECRETA:' in text:
            ending = re.findall(r'DECRETA:(.*)', text, re.S)
            if ending:
                text = ending[0]

        if 'CONSULTAR' in text:
            ending = re.findall(
                (r'(.*?)CONSULTAR\s*NOMBRES?\s*Y\s*FIRMAS?\s*'
                 r'(?:EN\s*ORIGINAL\s*IMPRESO\s*O)?\s*EN\s*FORMATO\s*PDF'),
                text, re.S)
            if ending:
                text = ending[0]

        if 'SENADO' in text:
            ending = re.findall(r'(.*?)SENADO\s*DE\s*LA\s*REP(?:Ú|U)BLICA',
                                text, re.S)
            if ending:
                text = ending[0]

        if 'REPRESENTANTES' in text:
            ending = re.findall(r'(.*?)C(?:Á|A)MARA\s*DE\s*REPRESENTANTES',
                                text, re.S)
            if ending:
                text = ending[0]

        articles = re.findall(r'(?!###)Artículo\s*(\d+)°\.(.*)', text)

        for n, a in articles:
            for r, f in _correct_final_re:
                a = re.sub(r, f, a).strip()

            clauses.append({
                'position': int(n) - 1,
                'empty': len(a) == 0,
                'markup': '<div>Artículo {0}</div><div>{1}</div><div><hr /></div>'.format(n, a)
            })

        if len(clauses) == 0:
            clauses.append({
                'position': 0,
                'empty': False,
                'markup': '<div>' + text + '</div><div><hr /></div>'
            })

        json_data = {
            'topicId': law_id,
            'links': [{
                'text': 'Ficha',
                'url': original,
            }],
            'forum': dos_forum_id,
            'tags': [_slugify(t) for t in tag.split() if len(t) > 4],
            'source': url,
            'tag': _slugify(tag),
            'owner': dos_user_id,
            'clauses': clauses,
            'mediaTitle': media_title.capitalize(),
            'publishedAt': publishedAt,
            'closingAt': closingAt,
            'action': {
                'count': 0,
                'method': args.m,
                'results': [{
                    'value': 'positive',
                    'percentage': 0
                }, {
                    'value': 'negative',
                    'percentage': 0
                }, {
                    'value': 'neutral',
                    'percentage': 0
                }]
            }
        }

        tag_json = {
            'hash': _slugify(tag),
            'name': tag.title()
        }

        json_filename = os.path.splitext(os.path.basename(pagename))[0] + '.json'
        json_path = os.path.join('json', json_filename)

        with open(json_path, 'w') as f:
            f.write(json.dumps(json_data, ensure_ascii=False, indent=4))

        tag_path = os.path.join('tags', _slugify(u(tag)) + '.json')

        if not os.path.exists(tag_path):
            with open(tag_path, 'w') as f:
                f.write(json.dumps(tag_json, ensure_ascii=False, indent=4))


def get_selectors(html, selector):
    with open(html) as f:
        body = f.read()
    doc = lxml.html.document_fromstring(body)
    sessions = doc.cssselect(selector)
    return sessions


def is_valid_url(url):
    regex = re.compile(
        r'^(http|ftp|file|https):///?'
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'
        r'localhost|[a-zA-Z0-9-]*|'
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
        r'(?::\d+)?'
        r'(?:/?|[/?]\S+)$',
        re.IGNORECASE
    )
    return regex.search(url)


def get_pagename(url, ext):
    u = urlparse(url)
    return ext + '/' + _slugify(os.path.basename(u.path) + u.query) + '.' + ext


def download_file(url, dest):
    print('Descargando ' + url)

    try:
        response = urlopen(url)
        response_read = response.read()
        time.sleep(1)
    except HTTPError as e:
        if str(e) is None:
            raise e
        if "404" in str(e):
            print(e)
            prefix = "http://qa.camara.gov.co/portal2011/"
            if url.startswith(prefix) and is_valid_url(url[len(prefix):]):
                download_file(url[len(prefix):], dest)
            return
        raise e
    except socket.error as e:
        if "reset" in e.strerror:
            print(e)
            time.sleep(5)
            download_file(url, dest)  # Try again
            return
        raise e
    except URLError as e:
        print(e)
        return

    with open(dest, 'wb') as f:
        f.write(response_read)


def update_url_params(url, params):
    url_parts = list(urlparse(url))
    query = dict(parse_qs(url_parts[4]))
    query.update(params)
    url_parts[4] = urlencode(OrderedDict(sorted(query.items())))
    return urlunparse(url_parts)


def scrape():
    print('Obteniendo páginas válidas ...')

    urlbase = 'http://qa.camara.gov.co/portal2011/proceso-y-tramite-legislativo/proyectos-de-ley'
    urlparams = {'view': 'proyectosdeley',
                 'option': 'com_proyectosdeley',
                 'limit': 0}
    page = update_url_params(urlbase, urlparams)
    pagename = get_pagename(page, 'html')

    laws = []
    projects = []

    if not os.path.exists(pagename):
        download_file(page, pagename)

    for session in get_selectors(pagename, 'a'):

        project = session.get('href')

        if project.startswith('/'):
            project = 'http://qa.camara.gov.co' + project

        link_query_dict = parse_qs(urlparse(project).query)

        if 'view' in link_query_dict and 'idpry' in link_query_dict:
            if link_query_dict['view'][0] == 'ver_proyectodeley':
                projects.append(project)

    if not int(args.n):
        nlaws = projects
    else:
        nlaws = projects[:int(args.n)]

    for project in nlaws:

        state = 'project'
        status = 'open'
        lawID = ''
        publishedAt = ''
        closingAt = ''
        tag = ''

        pagename = get_pagename(project, 'html')

        if not os.path.exists(pagename) and is_valid_url(project):
            download_file(project, pagename)

        for session in get_selectors(pagename, '.ar_12black b'):
            if _slugify(session.text) == 'ley':
                state = 'bill'

            if _slugify(session.text) in ['retirado', 'archivado', 'transito a comision']:
                status = 'closed'

        for i, session in enumerate(get_selectors(pagename, '.ar_12black')):
            if session.text:
                if i == 0:
                    lawID = session.text
                if i == 5:
                    date = session.text.split()
                    if date:
                        date[0] = _months[date[0]]
                        date.pop(2)
                        publishedAt = datetime.strptime(' '.join(date), '%B %d %Y').strftime('%Y-%m-%dT%H:%M:%S.000Z')
                    if status == 'closed' or state == 'bill':
                        closingAt = publishedAt
                if i == 7:
                    tag = session.text

        for session in get_selectors(pagename, 'a'):
            law = session.get('href')

            link_query_dict = parse_qs(urlparse(law).query)

            if 'p_tipo' in link_query_dict and is_valid_url(law):
                if int(link_query_dict['p_tipo'][0]) == 5:
                    if urlparse(law).netloc == 'www.imprenta.gov.co' \
                            and urlparse(law).path == '/gacetap/gaceta.mostrar_documento':
                        laws.append({
                            'original': project,
                            'url': law,
                            'tag': tag,
                            'lawID': lawID,
                            'publishedAt': publishedAt,
                            'closingAt': closingAt,
                            'state': state,
                            'status': status,
                            'pagename': get_pagename(law, 'text')
                        })

                    if urlparse(law).netloc == 'servoaspr.imprenta.gov.co:7778' \
                            and urlparse(law).path == '/gacetap/gaceta.mostrar_documento':
                        url_parts = list(urlparse(law))
                        url_parts[1] = url_parts[1].split(':')[0]
                        laws.append({
                            'original': project,
                            'url': urlunparse(url_parts),
                            'tag': tag,
                            'lawID': lawID,
                            'publishedAt': publishedAt,
                            'closingAt': closingAt,
                            'state': state,
                            'status': status,
                            'pagename': get_pagename(law, 'text')
                        })

    for law in laws:
        if not os.path.exists(law['pagename']):
            download_file(law['url'], law['pagename'])

        json_pagename = 'json/' + os.path.splitext(os.path.basename(law['pagename']))[0] + '.json'

        if not os.path.exists(json_pagename):
            text_to_json(**law)


def delete_directory_contents(directory):
    for file_path in os.listdir(directory):
        os.remove(os.path.join(directory, file_path))


if __name__ == "__main__":

    basedir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    topicsdir = os.path.join(basedir, 'json')
    tagsdir = os.path.join(basedir, 'tags')
    htmldir = os.path.join(basedir, 'html')
    textdir = os.path.join(basedir, 'text')

    for d in [topicsdir, tagsdir, htmldir, textdir]:
        if not os.path.exists(d):
            os.mkdir(d)
        if args.c:
            print('Eliminación de todos los archivos en directorio: ' + d)
            delete_directory_contents(d)

    initial_topics = os.listdir(topicsdir)
    initial_tags = os.listdir(tagsdir)

    scrape()

    if args.l:
        tags_to_insert = []
        topics_to_insert = []

        after_topics = os.listdir(topicsdir)
        after_tags = os.listdir(tagsdir)

        for tagfile in after_tags:
            if tagfile.endswith('.json'):
                tagpath = os.path.join(tagsdir, tagfile)
                if tagfile not in initial_tags or args.a:
                    with open(tagpath) as tagjson:
                        tag = json.loads(tagjson.read())

                    if list(mongoconn['tags'].find({'hash': tag['hash']})):
                        continue

                    mongoconn['tags'].insert_one(tag)

        for topicfile in after_topics:
            if topicfile.endswith('.json'):
                topicpath = os.path.join(topicsdir, topicfile)
                if topicfile not in initial_topics or args.a:
                    with open(topicpath) as topicjson:
                        topic = json.loads(topicjson.read())

                    dos_tag_query = mongoconn['tags'].find_one(
                        {'hash': topic['tag']})

                    if not dos_tag_query:
                        continue

                    dos_tag_id = dos_tag_query.get('_id')
                    topic['tag'] = ObjectId(dos_tag_id)
                    topic['forum'] = ObjectId(topic['forum'])
                    topic['owner'] = ObjectId(topic['owner'])
                    try:
                        topic['publishedAt'] = datetime.strptime(
                            topic['publishedAt'], '%Y-%m-%dT%H:%M:%S.000Z')
                    except Exception:
                        pass
                    if topic['closingAt']:
                        try:
                            topic['closingAt'] = datetime.strptime(
                                topic['closingAt'], '%Y-%m-%dT%H:%M:%S.000Z')
                        except Exception:
                            pass
                    else:
                        topic.pop('closingAt')

                    if list(mongoconn['topics'].find({'topicId': topic['topicId']})):
                        continue

                    mongoconn['topics'].insert_one(topic)
