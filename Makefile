#!/usr/bin/make -f
# -*- makefile -*-
#

# SHELL = sh -e

image:
	@docker build -t luisalejandro/dos-laws-scraper:latest .

scrape:
	@docker-compose up scraper

console:
	@docker-compose run --rm scraper bash

clean:
	@rm -rfv html/* json/* text/*
