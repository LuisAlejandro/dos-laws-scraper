FROM dockershelf/python:3.5
MAINTAINER Luis Alejandro Martínez Faneyth <luis@huntingbears.com.ve>

RUN apt-get update && \
   apt-get install sudo

RUN useradd -ms /bin/bash user

RUN echo "user ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/user

ADD requirements.txt /root/

RUN pip install -r /root/requirements.txt

RUN rm /root/requirements.txt

ADD . /home/user/app

RUN chown -R user:user /home/user

USER user

WORKDIR /home/user/app

CMD python scrape.py -l